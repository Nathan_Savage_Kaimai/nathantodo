import { Component, OnInit } from '@angular/core';
import { TaskService, task } from '../task-service.service';
import { map } from 'rxjs';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  tasks: task[] = []
  constructor(private service: TaskService) {}

  async ngOnInit() {
    (await this.service.getAllTasks()).pipe(
      map((tl) => {
        return tl.sort((a, b) => {
          if (a.complete && !b.complete) {
            return 1;
          } else if (!a.complete && b.complete) {
            return -1;
          }
          return 0;
        });
      })
    ).subscribe((tl) => {this.tasks = tl})
  }

  async toggleTask(id: number): Promise<void> {
    this.service.toggle(id)
  }

  async removeTask(id: number): Promise<void> {
    this.service.remove(id)
  }
}
