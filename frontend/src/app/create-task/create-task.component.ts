import { Component } from '@angular/core';
import { TaskService } from '../task-service.service';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent {
  titleFormControl = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(25)]);
  descriptionFormControl = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor(private service: TaskService, private router: Router) {

  }

  async addTask() {
    this.service.addTask(this.titleFormControl.value as string, this.descriptionFormControl.value as string)
    return this.router.navigate([''])
  }
}
