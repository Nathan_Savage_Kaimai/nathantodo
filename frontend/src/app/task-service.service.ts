import { Injectable } from '@angular/core';
import { BehaviorSubject, lastValueFrom } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export type task = {id: number, title: string, description: string, complete: boolean}

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  host = "http://localhost:3000"

  tasks: BehaviorSubject<task[]> = new BehaviorSubject([] as task[])

  constructor(private client: HttpClient) { }

  async getAllTasks(): Promise<BehaviorSubject<task[]>> {
    await this.refreshTasks()

    return this.tasks
  }

  async refreshTasks(): Promise<void> {
    await this.client.get(`${this.host}/getTasks`, {headers: {'Access-Control-Allow-Origin': '*'}}).subscribe((tasks) => {this.tasks.next(tasks as task[])})
  }

  async toggle(id: number): Promise<void> {
    await lastValueFrom( this.client.post(`${this.host}/task/${id}/toggle`, {}, {headers: {'Access-Control-Allow-Origin': '*'}}) )

    this.refreshTasks()
  }

  async remove(id: number): Promise<void> {
    await lastValueFrom( this.client.post(`${this.host}/task/${id}/remove`, {}, {headers: {'Access-Control-Allow-Origin': '*'}}) )

    this.refreshTasks()
  }

  async addTask(title: string, description: string): Promise<void> {
     await lastValueFrom( this.client.post(`${this.host}/task/add`, {title: title, description: description}, {headers: {'Access-Control-Allow-Origin': '*'}}) )

    this.refreshTasks()
  }
}
