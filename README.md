# To Install and Run

First clone the repo and do one of the following:

## If you have docker installed
Run 
```
docker compose up
```

## Otherwise
Go to the frontend dir and run:
```
npm install
npm run start
```
Go to the backend dir and run:
```
npm install
npm run start
```

# To Use

Once installed, go to [http://localhost:4200](http://localhost:4200)