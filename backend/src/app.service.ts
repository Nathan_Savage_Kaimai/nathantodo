import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from './todo.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Todo)
    private todoRepository: Repository<Todo>
  ) {}

  async getAllTasks(): Promise<Todo[]> {
    return this.todoRepository.find({select: ['id', 'title', 'description', 'complete'], order: {createdAt: 'DESC'}});
  }
  
  async toggleTask(rawId: string): Promise<void> {
    const id = this.idSanitise(rawId)
    const task = await this.todoRepository.findOneOrFail({where: {id: id}})
    task.complete = !task.complete
    this.todoRepository.save(task)
  }

  async removeTask(rawId: string): Promise<void> {
    const id = this.idSanitise(rawId)
    const task = await this.todoRepository.findOneOrFail({where: {id: id}})
    await this.todoRepository.remove(task)
  }

  async addTask(rawTitle: string, rawDescription: string): Promise<void> {
    if(rawTitle.length <= 2 ||  rawTitle.length >= 26 || rawDescription.length <= 2) {
      throw new BadRequestException('title must be between 3 and 25 characters and the description more than 3')
    }
    const task = new Todo()
    task.title = rawTitle
    task.description = rawDescription
    await this.todoRepository.save(task)
  }

  idSanitise(rawId: string): number {
    return Number.parseInt(rawId)
  }
}
