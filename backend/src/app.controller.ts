import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { Todo } from './todo.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('getTasks')
  async getAllTasks(): Promise<Todo[]> {
    return this.appService.getAllTasks();
  }

  @Post('task/:id/toggle')
  async toggleTask(@Param('id') rawId: string): Promise<void> {
    this.appService.toggleTask(rawId)
  }

  @Post('task/:id/remove')
  async removeTask(@Param('id') rawId: string): Promise<void> {
    this.appService.removeTask(rawId)
  }

  @Post('task/add')
  async addTask(@Body('title') rawTitle: string, @Body('description') rawDescription: string): Promise<void> {
    this.appService.addTask(rawTitle, rawDescription)
  }
}
